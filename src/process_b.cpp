#include <bondcpp/bond.h>
#include <ros/spinner.h>

int main(int argc, char **argv)
{
  ros::init(argc, argv, "ProcessB", true);
  ros::AsyncSpinner spinner(1);
  spinner.start();

  bond::Bond bond("example_bond_topic", "myBondId123456");
  bond.start();
  // ... do things ...

  fprintf(stdout, "Bond started\n");
  for (size_t i = 0; i < 2000000000; i++) {} 
  fprintf(stdout, "Breaking bond\n");
  bond.breakBond();
  fprintf(stdout, "Bond stopped\n");
}
