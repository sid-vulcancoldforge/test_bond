#include <bondcpp/bond.h>
#include <ros/spinner.h>

int main(int argc, char **argv)
{
  ros::init(argc, argv, "ProcessA", true);
  ros::AsyncSpinner spinner(1);
  spinner.start();
  bond::Bond *bond = new bond::Bond("example_bond_topic", "myBondId123456");
  ROS_INFO("A starting bond");
  bond->start();
  ROS_INFO("A waiting for bond to be formed");
  if (!bond->waitUntilFormed(ros::Duration(10.0))) {
      ROS_ERROR("ERROR!");
      return false;
  }
  ROS_INFO("A waiting for bond to be broken");
  // ... do things with B ...
  bond->waitUntilBroken(ros::Duration(20.0));
  ROS_INFO("B has broken the bond");
  ROS_INFO("A starting bond again");
  delete bond;
  bond = new bond::Bond("example_bond_topic", "myBondId123456");
  bond->start();
  ROS_INFO("A waiting for bond to be formed again");
  if (!bond->waitUntilFormed(ros::Duration(10.0)))
  {
      ROS_ERROR("ERROR!");
      return false;
  }
  ROS_INFO("A waiting for bond to be broken again");
  bond->waitUntilBroken(ros::Duration(20.0));
  ROS_INFO("B has broken the bond");
}
